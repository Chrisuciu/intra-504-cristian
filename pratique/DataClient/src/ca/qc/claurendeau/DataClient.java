package ca.qc.claurendeau;


import ca.qc.claurendeau.exceptions.DataException;
import ca.qc.claurendeau.exceptions.DatabaseException;
import ca.qc.claurendeau.exceptions.NetworkException;

public class DataClient
{
	
	DataServer dataServer;
	
	public DataClient(DataServer dataServer)
	{
		dataServer = new DataServer();
	}

	@SuppressWarnings("finally")
	public int processData(int entryID)
	{
		int voltage = -1;
		String csvData = null;
		int inputLeft;
		int inputRight;
		int coefficient;

		try {
			// initiateNewTransaction
			initiateNewTransaction(entryID);
			
			// getCSVData
			csvData = getCsvData();
			
			// extraction des données à partir de getCSVData
			// un format csv est un tableau séparer par des , sous un format string
			String[] data = extractData(csvData);
			inputLeft = Integer.parseInt(data[0]);
			inputRight = Integer.parseInt(data[1]);
			coefficient = Integer.parseInt(data[2]);
			
			// calcul de output (output = inputLeft + inputRight)
			int output = calculOutput(inputLeft, inputRight);
			
			// si okToSendVoltage(output, coefficient)
			// sendVoltage(output, coefficient)
			if(okToSendVoltage(output, coefficient)){
				sendVoltage(output, coefficient);
				voltage = output * coefficient;
			}
			
			finalizeTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
		} catch (DataException e) {
			e.printStackTrace();
		} catch (NetworkException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// retourne -1 si aucun voltage envoyé
			return voltage;
		}
	}
	
	public void initiateNewTransaction(int id) throws DatabaseException {
		this.dataServer.initiateNewTransaction(id);
	}
	
	public String getCsvData() throws DataException {
		return this.dataServer.getCSVData();
	}
	
	public String[] extractData(String csvData) {
		return csvData.split(",", 0);
	}
	
	public int calculOutput(int inputLeft, int inputRight) {
		return inputLeft + inputRight;
	}
	
	public boolean okToSendVoltage(int output, int coefficient) {
		return this.dataServer.okToSendVoltage(output, coefficient);
	}
	
	public void sendVoltage(int output, int coefficient) throws NetworkException {
		this.dataServer.sendVoltage(output, coefficient);
	}
	
	public void finalizeTransaction() throws DatabaseException {
		this.dataServer.finalizeTransaction();
	}
}

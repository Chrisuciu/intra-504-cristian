package ca.qc.claurendeau;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import ca.qc.claurendeau.exceptions.DataException;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;

// Testez votre code à l'aide de tests unitaires et de doublures
// Utilisez Mockito pour les doublures

@RunWith(MockitoJUnitRunner.class)
public class DataClientTest {

	@Mock
	DataServer dataServer;
	DataClient dataClient;
	int transactionId;
	int output;
	int coefficient;
	
	@Before
	public void init() {
		dataClient = new DataClient(dataServer);
		transactionId = 0;
		output = 18;
		coefficient = 5;
	}
	
	@Test
	public <T> void processDataMocked() {
		try {
			// Mockito.doNothing().when(dataClient).initiateNewTransaction(transactionId);
			// Mockito.doNothing().when(dataClient).finalizeTransaction();
			// Je ne sais pas comment mocker du void
			// Ayant su comment mocker du void, j'aurais créer un test pour chaque type 
			// d'exception possible à déclencher
			Mockito.when(dataClient.getCsvData()).thenReturn("10,8,5");
			Mockito.when(dataClient.okToSendVoltage(output, coefficient)).thenReturn(true);
			int result = dataClient.processData(transactionId);
			assertTrue(result == 90 );
		} catch (DataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	@Ignore
	public void processDataDataException(){
		
	}
	
	@Test
	@Ignore
	public void processDataDataBaseException(){
		
	}
	
	@Test
	@Ignore
	public void processDataNetworkException(){
		
	}
}
